window.addEventListener("load", function(){
window.cookieconsent.initialise({
  content: {
    header: 'Cookies utilizados no site',
    message: 'Estamos usando cookies para personalizar conteúdo e anúncios para facilitar o uso de nosso site.',
    dismiss: 'Confirmar',
    allow: 'Habilitar Cookies',
    deny: 'Recusar',
    link: 'Saiba mais',
    href: 'http://cookiesandyou.com',
    close: '&#x274c;',
  },
  cookie: {
    expiryDays: 365
  },
  position: 'top'
});
$(".cc-banner").wrapInner("<div class='cc-container container'></div>");
});