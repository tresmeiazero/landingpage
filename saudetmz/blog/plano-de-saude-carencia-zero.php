<!DOCTYPE html>
<html lang="en">
<head>
  <title>Planos de Saúde 360 | Plano de Saúde com carência zero existe ou não existe?</title>

    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700' rel='stylesheet'>

    <!-- Css -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="../css/font-icons.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <link rel="stylesheet" href="../css/style.css" />
    <meta property="og:url" content="http://landingpages.planosdesaude360.com.br/saude360/blog/plano-de-saude-carencia-zero.php" />
    <meta property="og:type"               content="article" />
    <meta property="og:title" content="Plano de Saúde com carência zero existe ou não existe?" />
    <meta property="og:description" content="Um dos maiores desejos dos brasileiros é ter um plano de saúde carência zero. Seja para tratar alguma doença, ou para simplesmente ter uma segurança para si ou para toda a família." />
    <meta property="og:image" content="https://landingpages.planosdesaude360.com.br/saude360/img/blog/imgDestaqueBlogPostCarenciaZero.png" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="../img/favicon.ico">
    <link rel="apple-touch-icon" href="../img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../img/apple-touch-icon-114x114.png">
    <script type="text/javascript">
        (function(p,u,s,h){
            p._pcq=p._pcq||[];
            p._pcq.push(['_currentTime',Date.now()]);
            s=u.createElement('script');
            s.type='text/javascript';
            s.async=true;
            s.src='https://cdn.pushcrew.com/js/bc0842e12b6abdc003db3eab8145b1c0.js';
            h=u.getElementsByTagName('script')[0];
            h.parentNode.insertBefore(s,h);
        })(window,document);
    </script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.0&appId=189387035006802&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <script src="//code.tidio.co/qvbl26alebsnjlsexlsszypqb6fkgsmo.js"></script>
    <?php include("../includes/IncludesConexaoBanco.php"); ?>
    <?php include("../includes/IncludesPixel.php"); ?>


</head>

<body class="blog">

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      "Loading..."
    </div>
  </div>

  <main class="main-wrapper">

      <!-- Navigation -->
      <header class="nav">
          <div class="nav__holder nav--sticky">
              <div class="container-fluid container-semi-fluid nav__container">
                  <div class="flex-parent">

                      <div class="nav__header">
                          <!-- Logo -->
                          <a href="../index.php" class="logo-container flex-child">
                              <img class="logo" src="../img/logo.png" srcset="../img/logo.png 1x, ../img/logo@2x.png 2x" alt="logo">
                          </a>

                          <!-- Mobile toggle -->
                          <button type="button" class="nav__icon-toggle" id="nav__icon-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                          </button>
                      </div>

                      <!-- Navbar -->
                      <nav id="navbar-collapse" class="nav__wrap collapse navbar-collapse">
                          <ul class="nav__menu">
                              <li class="active">
                                  <a href="index.php">Home</a>
                              </li>
                              <li class="nav__dropdown">
                                  <a href="index.php#planos">Planos</a>
                              </li>
                              <li class="nav__dropdown">
                                  <a href="index.php#sobrenos">Sobre</a>

                              </li>
                              <li class="nav__dropdown">
                                  <a href="blog.php">Blog</a>

                              </li>
                              <li>
                                  <a href="#">Contato</a>
                              </li>
                          </ul> <!-- end menu -->
                      </nav> <!-- end nav-wrap -->

                      <div class="nav__btn-holder nav--align-right">
                          <a href="#" class="btn nav__btn botaoshake">
                              <span class="nav__btn-text">Entre em contato</span>
                              <span class="nav__btn-phone">Clique aqui</span>
                          </a>
                      </div>

                  </div> <!-- end flex-parent -->
              </div> <!-- end container -->

          </div>
      </header> <!-- end navigation -->

    <div class="content-wrapper oh">

      <!-- Page Title -->
      <section class="page-title blog-featured-img bg-color-overlay bg-color-overlay--1 text-center" style="background-image: url(../img/blog/imgDestaque01.jpg);">
        <div class="container">
          <div class="page-title__holder">
            <h1 class="page-title__title">Plano de Saúde carência zero existe ou não?</h1>
          </div>
        </div>
      </section> <!-- end page title -->

      <!-- Single Post -->
      <section class="section-wrap pt-40 pb-48">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8">
              <div class="blog__content">
                <article class="entry mb-0">
                  <div class="entry__article-wrap">

                    <!-- Share -->
                    <div class="entry__share">
                      <div class="sticky-col">
                        <div class="socials socials--rounded socials--large">
                            <div class="fb-share-button" data-href="http://landingpages.planosdesaude360.com.br/saude360/blog/plano-de-saude-carencia-zero.php" data-layout="button" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flandingpages.planosdesaude360.com.br%2Fsaude360%2Fblog%2Fplano-de-saude-carencia-zero.php&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Compartilhar</a></div>


                        </div>
                      </div>                  
                    </div> <!-- share -->

                    <div class="entry__article">
                      <ul class="entry__meta">
                        <li class="entry__meta-category">
                          <i class="ui-category"></i>
                          <a href="#">Planos de Saúde</a>
                        </li>
                        <li class="entry__meta-star">
                          <i class="ui-star"></i>
                          <a href="#">Leitura: 5 Min</a>
                        </li>
                      </ul>

                      <p>Um dos maiores desejos dos brasileiros é ter um plano de saúde carência zero. Seja para tratar alguma doença, ou para simplesmente ter uma segurança para si ou para toda a família.</p>

                      <p> Porém, com a falta de informação, algumas pessoas, querendo mais benefícios, são enganadas pelo famoso plano de saúde carência zero.</p>

                      <p>Mas será mesmo que existe carência zero nas operadoras de saúde? Confira nesse artigo.</p>

                      <h3>O que é a carência em um plano de saúde? </h3>

                      <figure class="alignleft">
                        <img src="../img/blog/post_img_1.jpg" alt="">
                        <figcaption>O que é carência?</figcaption>
                      </figure>

                      <p> A carência, dentro de um plano de saúde, nada mais é do que o período que a pessoa precisa esperar a partir de sua contratação para utilizar um determinado procedimento que é oferecido pelo plano de saúde.</p>

                      <p> Por exemplo: você contrata um plano de saúde no dia 10 de janeiro, e a partir dessa data podem existir diferentes prazos de carência.</p>

                      <p> Geralmente, exames laboratoriais podem ser usados a partir de 30 dias de contratação do plano, consultas também tendem a seguir esse mesmo prazo.</p>

                      <p> Dessa forma então, em um caso de contratação de um plano de saúde, que tenha 30 dias de carência, e foi contratado no dia 10 janeiro, seu contratante só poderá utilizar os serviços no dia 20 de fevereiro.</p>



                      <h4>Quais são as principais carências?</h4>
                      <p> De acordo com a ANS – Agência Nacional de Saúde Suplementar – as carências permitidas por lei, nos planos de saúde que atuam no Brasil, são:</p>

                      <ul>
                        <li>Exames laboratoriais</li>
                        <li>Consultas médicas</li>
                        <li>Acidentes que necessitem urgência ou emergência</li>
                        <li>Partos</li>
                        <li>Cirurgias</li>
                      </ul>

                      <h3>Prazos de carência para os planos de saúde</h3>

                      <figure class="alignright">
                        <img src="../img/blog/post_img_2.jpg" alt="">
                        <figcaption>Prazos de carência </figcaption>
                      </figure>  

                      <p>Com base na lei válida a partir de 02 de janeiro de 1999, o plano de saúde pode exigir os seguintes prazos de carência para os determinados procedimentos:</p>

                      <ul>
                        <li>Em casos de urgência e emergência, como acidentes pessoais, complicações gestacionais, lesões irreparáveis que impliquem risco imediato à vida, o prazo de carência é de 24 horas</li>
                        <li>Para realizar exames laboratoriais e/ou agendar consultas médicas com especialistas, o prazo de carência é de 30 dias corridos</li>
                        <li>Em casos de partos a termo – onde exclui-se partos prematuros ou então que sejam decorrentes de complicações durante o período gestacional, o prazo é de 300 dias</li>
                        <li>Para tratamento de lesões pré-existentes, ou seja, quando plano de saúde e paciente já tem o conhecimento do problema, o prazo pode chegar a 24 meses</li>
                        <li>Para demais situações, os prazos são de 180 dias</li>
                      </ul>                  

                      <h6>Existe plano de saúde carência zero?</h6>

                      <p> Em meio a tantos planos de saúde, as pessoas ficam sem saber qual devam contratar. Isso é normal, afinal são muitas opções e benefícios.</p>

                      <p> Mas você deve tomar muito cuidado com a propaganda de plano de saúde carência zero, pois todos os planos individuais devem seguir às regras de carências impostas pela ANS.</p>

                      <p> Os únicos que podem oferecer plano de saúde carência zero são os Coletivos por Adesão e os Empresariais, sendo assim, os beneficiários destes não precisam cumprir os prazos.</p>

                      <p> A pesquisadora em saúde do Idec – Instituto Brasileiro de Defesa do Consumidor, Ana Carolina Navarrete, diz que “O consumidor deve desconfiar se oferecerem para ele entrar em um plano coletivo de alguma empresa ou mesmo entidade de classe que ele desconheça ou que não tenha nada a ver com a entidade que o representa”.</p>

                      <p> Portanto, plano de saúde carência zero para planos individuais não existe e, ao contratar um plano com essa promessa, você deve se certificar se a informação é, de fato, verdadeira.</p>

                      <p> Caso o contratante do plano for enganado depois da contratação, ele deverá organizar todas as provas de que foi lesado e notificar à ANS, através do Fale Conosco.</p>

                      <h3>Se não existe plano de saúde carência zero, qual escolher?</h3>

                      <figure class="alignleft">
                        <img src="../img/blog/post_img_3.jpg" alt="">
                        <figcaption>Qual plano escolher?</figcaption>
                      </figure>

                      <p>  Essa é uma pergunta feita por muitas pessoas que desejam contratar um plano de saúde com segurança. </p>

                      <p> E se você não cumpre os requisitos de possuir um plano coletivo por adesão ou empresarial, deve pesquisar bem. </p>

                      <p> Antes de mais nada, certifique-se que seu corretor de plano de saúde está habilitado pela Susep – Superintendência de Seguros Privados – regulamentadora da profissão. Esse é o primeiro passo para evitar qualquer fraude. </p>

                      <p> E, antes de fechar negócio, atente-se aos diversos planos de saúde presentes em sua região, pesquise sobre valores, coberturas e principalmente sobre períodos de carência.

                      <p> Todas as dúvidas devem ser sanadas antes de um negócio ser fechado. </p>

                      <p>  Nosso site disponibiliza corretores credenciados que possibilita uma maior segurança na contratação do seu plano de saúde. Clique aqui para conversar com um deles. </p>

                      <p>  Não brinque com sua saúde, nem com a saúde de sua família, para estar sempre seguro, contrate um plano de saúde. </p>

                      <!-- tags -->
                      <div class="entry__tags">
                        <i class="ui-tags"></i>
                        <span class="entry__tags-label">Fonte:</span>
                        <a href="#" rel="tag">Uol </a>

                      </div> <!-- end tags -->

                    </div> <!-- end entry article -->
                  </div> <!-- end entry article wrap -->
                </article>

                <section class="related-posts">
                  <h5 class="mb-24">Veja Também</h5>
                  <div class="row row-16 card-row">
                    <div class="col-lg-4">
                      <article class="entry card card--small box-shadow hover-up">
                        <div class="entry__img-holder card__img-holder">
                          <a href="plano-de-saude-carencia-zero.php">
                            <img src="../img/blog/post_1.jpg" class="entry__img" alt="">
                          </a>
                          <div class="entry__body card__body">
                            <h4 class="entry__title">
                              <a href="plano-de-saude-carencia-zero.php">Plano de Saúde carência zero existe ou não?</a>
                            </h4>
                          </div>
                        </div>
                      </article>
                    </div>
                    <div class="col-lg-4">
                      <article class="entry card card--small box-shadow hover-up">
                        <div class="entry__img-holder card__img-holder">
                          <a href="como-fazer-um-plano-de-saude-empresarial.php">
                            <img src="../img/blog/post_2.jpg" class="entry__img" alt="">
                          </a>
                          <div class="entry__body card__body">
                            <h4 class="entry__title">
                              <a href="como-fazer-um-plano-de-saude-empresarial.php">Como fazer um Plano de Saúde Empresarial</a>
                            </h4>
                          </div>
                        </div>
                      </article>
                    </div>
                    <div class="col-lg-4">
                      <article class="entry card card--small box-shadow hover-up">
                        <div class="entry__img-holder card__img-holder">
                          <a href="plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">
                            <img src="../img/blog/post_3.jpg" class="entry__img" alt="">
                          </a>
                          <div class="entry__body card__body">
                            <h4 class="entry__title">
                              <a href="plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">Plano de Saúde cobre ou não cirurgia plástica?
                              </a>
                            </h4>
                          </div>
                        </div>
                      </article>
                    </div>
                  </div>
                </section>
                

                <!-- Comments -->



                <!-- Prev / Next Post -->
                <nav class="entry-navigation">
                  <div class="row">
                    <div class="col-lg-6">
                      <a href="plano-de-saude-cobre-ou-nao-cirurgia-plastica.php" class="entry-navigation__url entry-navigation--left">
                        <img src="../img/blog/next_post-cirurgias.jpg" alt="" class="entry-navigation__img">
                        <div class="entry-navigation__body">
                          <i class="ui-arrow-left"></i>
                          <span class="entry-navigation__label">Post Anterior</span>
                          <h6 class="entry-navigation__title">Plano de Saúde cobre ou não cirurgia plástica?</h6>
                        </div>
                      </a>
                    </div>
                    <div class="col-lg-6">
                      <a href="como-fazer-um-plano-de-saude-empresarial.php" class="entry-navigation__url entry-navigation--right">
                        <div class="entry-navigation__body">
                          <span class="entry-navigation__label">Próximo Post</span>
                          <i class="ui-arrow-right"></i>
                          <h6 class="entry-navigation__title">Como fazer um Plano de Saúde Empresarial</h6>
                        </div>
                        <img src="../img/blog/next_post-pme.jpg" alt="" class="entry-navigation__img">
                      </a>
                    </div>
                  </div>
                </nav>

              </div>
            </div>
          </div>
        </div>
      </section> <!-- end single post -->


        <!-- Newsletter -->
        <section class="section-wrap newsletter bg-color-overlay--2" style="background-image: url(../img/newsletter/newsletter.jpg);">
            <div class="container">
                <div class="title-row title-row--boxed text-center">
                    <h3 class="section-title">Cote online</h3>
                    <p class="subtitle">Preencha o formulário e cote online</p>
                </div>
                <div class="widget widget_mc4wp_form_widget">
                    <div class="newsletter__container">
                        <div class="newsletter__form">
                            <?php include("../includes/IncludesInsereBanco.php"); ?>
                            <form class="optin__form" role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #fff">
                                <?php echo $msgClientesSucesso; ?>
                                <?php echo $msgClientesErro; ?>
                                <?php echo $e; ?>
                                <?php include("../includes/includeForm.php"); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section> <!-- end newsletter -->


      <!-- Footer -->
      <footer class="footer">
        <div class="container">
          <div class="footer__widgets">
            <div class="row">

              <div class="col-lg-3 col-md-6">
                <div class="widget widget-about-us">
                  <!-- Logo -->
                  <a href="index.php" class="logo-container flex-child">
                    <img class="logo" src="../img/logo.png" srcset="../img/logo.png 1x, ../img/logo@2x.png 2x" alt="logo">
                  </a>
                  <p class="mt-24 mb-32">Encontre o plano de saúde que mais se encaixa na sua necessidade.</p>
                  <div class="socials">
                    <a href="#" class="social social-twitter" aria-label="twitter" title="twitter" target="_blank"><i class="ui-twitter"></i></a>
                    <a href="#" class="social social-facebook" aria-label="facebook" title="facebook" target="_blank"><i class="ui-facebook"></i></a>
                    <a href="#" class="social social-google-plus" aria-label="google plus" title="google plus" target="_blank"><i class="ui-google"></i></a>
                  </div>
                </div>
              </div> <!-- end about us -->


              <div class="col-lg-2 offset-lg-5 col-md-6">
                <div class="widget widget_nav_menu">
                  <h5 class="widget-title">Saúde 360</h5>
                  <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Planos</a></li>
                    <li><a href="#">Sobre</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contato</a></li>
                  </ul>
                </div>
              </div>



              <div class="col-lg-2 col-md-6">
                <div class="widget widget-address">
                  <h5 class="widget-title">Operadoras</h5>
                  <ul>
                    <li><a href="#">Amil</a></li>
                    <li><a href="#">Bradesco</a></li>
                    <li><a href="#">Sulamerica</a></li>
                    <li><a href="#">Intermedica</a></li>
                  </ul>
                </div>
              </div>

            </div>
          </div>
        </div> <!-- end container -->

        <div class="footer__bottom top-divider">
          <div class="container text-center">
            <span class="copyright">
              &copy; 2018 Saúde 360, Desenvolvido por <a href="https://agenciatresmeiazero.com.br">Agência TresMeiaZero</a>
            </span>
          </div>
        </div> <!-- end footer bottom -->
      </footer> <!-- end footer -->

      <div id="back-to-top">
        <a href="#top"><i class="ui-arrow-up"></i></a>
      </div>

    </div> <!-- end content wrapper -->
  </main> <!-- end main wrapper -->


  <?php include("../includes/includesFooter.php"); ?>

  
</body>
</html>