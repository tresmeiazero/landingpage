<!DOCTYPE html>
<html lang="en">
<head>
  <title>Planos de Saúde 360 | Plano de Saúde Cobre Cirurgia Plástica?</title>

    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700' rel='stylesheet'>

    <!-- Css -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="../css/font-icons.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <link rel="stylesheet" href="../css/style.css" />
    <meta property="og:url" content="http://landingpages.planosdesaude360.com.br/saude360/blog/plano-de-saude-carencia-zero.php" />
    <meta property="og:type"               content="article" />
    <meta property="og:title" content="Plano de Saúde cobre ou não cirurgia plástica?" />
    <meta property="og:description" content="O Brasil é o segundo país do mundo em quantidade de cirurgia plástica, com mais de 1,22 milhão de procedimentos realizados, perdendo apenas para os Estados Unidos, que somam mais de 1,41 milhão." />
    <meta property="og:image" content="https://landingpages.planosdesaude360.com.br/saude360/img/blog/imgDestaqueBlogPostCirurgiaPlastica.png" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="../img/favicon.ico">
    <link rel="apple-touch-icon" href="../img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../img/apple-touch-icon-114x114.png">
    <script type="text/javascript">
        (function(p,u,s,h){
            p._pcq=p._pcq||[];
            p._pcq.push(['_currentTime',Date.now()]);
            s=u.createElement('script');
            s.type='text/javascript';
            s.async=true;
            s.src='https://cdn.pushcrew.com/js/bc0842e12b6abdc003db3eab8145b1c0.js';
            h=u.getElementsByTagName('script')[0];
            h.parentNode.insertBefore(s,h);
        })(window,document);
    </script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.0&appId=189387035006802&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <script src="//code.tidio.co/qvbl26alebsnjlsexlsszypqb6fkgsmo.js"></script>
    <?php include("../includes/IncludesConexaoBanco.php"); ?>
    <?php include("../includes/IncludesPixel.php"); ?>

</head>

<body class="blog">

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      "Loading..."
    </div>
  </div>

  <main class="main-wrapper">

      <!-- Navigation -->
      <header class="nav">
          <div class="nav__holder nav--sticky">
              <div class="container-fluid container-semi-fluid nav__container">
                  <div class="flex-parent">

                      <div class="nav__header">
                          <!-- Logo -->
                          <a href="../index.php" class="logo-container flex-child">
                              <img class="logo" src="../img/logo.png" srcset="../img/logo.png 1x, ../img/logo@2x.png 2x" alt="logo">
                          </a>

                          <!-- Mobile toggle -->
                          <button type="button" class="nav__icon-toggle" id="nav__icon-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                          </button>
                      </div>

                      <!-- Navbar -->
                      <nav id="navbar-collapse" class="nav__wrap collapse navbar-collapse">
                          <ul class="nav__menu">
                              <li class="active">
                                  <a href="index.php">Home</a>
                              </li>
                              <li class="nav__dropdown">
                                  <a href="index.php#planos">Planos</a>
                              </li>
                              <li class="nav__dropdown">
                                  <a href="index.php#sobrenos">Sobre</a>

                              </li>
                              <li class="nav__dropdown">
                                  <a href="blog.php">Blog</a>

                              </li>
                              <li>
                                  <a href="#">Contato</a>
                              </li>
                          </ul> <!-- end menu -->
                      </nav> <!-- end nav-wrap -->

                      <div class="nav__btn-holder nav--align-right">
                          <a href="#" class="btn nav__btn botaoshake">
                              <span class="nav__btn-text">Entre em contato</span>
                              <span class="nav__btn-phone">Clique aqui</span>
                          </a>
                      </div>

                  </div> <!-- end flex-parent -->
              </div> <!-- end container -->

          </div>
      </header> <!-- end navigation -->

    <div class="content-wrapper oh">

      <!-- Page Title -->
      <section class="page-title blog-featured-img bg-color-overlay bg-color-overlay--1 text-center" style="background-image: url(../img/blog/imgDestaque03.jpg);">
        <div class="container">
          <div class="page-title__holder">
            <h1 class="page-title__title">Plano de Saúde cobre ou não cirurgia plástica?</h1>
          </div>
        </div>
      </section> <!-- end page title -->

      <!-- Single Post -->
      <section class="section-wrap pt-40 pb-48">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8">
              <div class="blog__content">
                <article class="entry mb-0">
                  <div class="entry__article-wrap">

                    <!-- Share -->
                    <div class="entry__share">
                      <div class="sticky-col">
                        <div class="socials socials--rounded socials--large">
                            <div class="fb-share-button" data-href="http://landingpages.planosdesaude360.com.br/saude360/blog/plano-de-saude-cobre-ou-nao-cirurgia-plastica.php" data-layout="button" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flandingpages.planosdesaude360.com.br%2Fsaude360%2Fblog%2Fplano-de-saude-cobre-ou-nao-cirurgia-plastica.php&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Compartilhar</a></div>
                        </div>
                      </div>                  
                    </div> <!-- share -->

                    <div class="entry__article">
                      <ul class="entry__meta">
                        <li class="entry__meta-category">
                          <i class="ui-category"></i>
                          <a href="#">Planos de Saúde</a>
                        </li>
                        <li class="entry__meta-star">
                          <i class="ui-star"></i>
                          <a href="#">Leitura: 6 Min</a>
                        </li>
                      </ul>

                      <p>O Brasil é o segundo país do mundo em quantidade de cirurgia plástica, com mais de 1,22 milhão de procedimentos realizados, perdendo apenas para os Estados Unidos, que somam mais de 1,41 milhão.</p>

                      <p> Sonho da maioria absoluta das mulheres e desejo secreto (mas cada vez mais assumido) de muitos homens, os mais comuns são a lipoaspiração, o implante de silicone nos seios, a cirurgia de pálpebra (blefaroplastia), e a de abdome (abdominoplastia).</p>

                      <p> Mas será que os planos de saúde cobrem cirurgia plástica? Em que situações é possível aproveitar os benefícios do plano de saúde?</p>

                      <p> Se esse assunto é do seu interesse, preste bastante atenção para ver se você se encaixa em algum desses casos.</p>


                      <h4>Planos de saúde não são obrigados a cobrir cirurgia plástica estética</h4>

                      <figure class="alignleft">
                        <img src="../img/blog/post_img_6.jpg" alt="">
                        <figcaption>O que é obrigatório?</figcaption>
                      </figure>

                      <p>Essa é a mais pura verdade, mas se você prestar atenção, há uma pegadinha nessa frase. Na verdade, mais de uma. A primeira delas é que eles não são obrigados – mas também não são proibidos.</p>

                      <p>E como nem todos os planos de saúde são iguais, se a operadora quiser cobrir o procedimento, basta ela incluir a cobertura para cirurgia plástica, servindo como um diferencial frente à concorrência.</p>

                      <p>Isso porque, apesar de haver alumas regras em comum a todos eles, cada plano pode fazer suas próprias regras, que podem mudar conforme o tempo.</p>

                      <p> Nesse caso, a dica é conhecer bem o plano de saúde antes de fazer a contratação. Converse com o vendedor especializado, explique a ele seus desejos e necessidades – e, principalmente, leia todas as cláusulas do contrato para não ter surpresas desagradáveis mais tarde.</p>

                      <p>Se o plano promete a cobertura de cirurgias plásticas, tenha certeza de que essa promessa estará clara no contrato e também se a cirurgia plástica não está relacionada apenas à necessidade em caso de saúde.</p>

                      <p>E é justamente daí que vem a outra pegadinha, dessa vez em relação à palavra estética. A Agência Nacional de Saúde (ANS) realmente não reconhece a obrigatoriedade de realização de procedimentos estéticos, a não ser quando a cirurgia plástica está relacionada a alguma questão de saúde. E isso é lei: a 9.656/98 lista alguns casos bem específicos.</p>

                      <p>Um deles é quando a cirurgia plástica é para a retirada do excesso de pele para os pacientes que fizeram cirurgia de redução do estômago.</p>

                      <p> Como esse excesso compromete sua saúde e o dia a dia do beneficiário, ele tem direito à cirurgia plástica sem qualquer ônus.</p>

                      <figure class="alignright">
                        <img src="../img/blog/post_img_7.jpg" alt="">
                        <figcaption>Reconstrução da mama  </figcaption>
                      </figure>

                      <p>Outra cirurgia plástica incluída na cobertura obrigatória é a de reconstrução da mama para mulheres que fizeram masectomia.</p>

                      <p> A lei prevê a cobertura para beneficiários com diagnóstico de câncer de mama, lesões traumáticas e tumores em geral – inclusive  cobertura da mastoplastia na mama oposta após reconstrução da contralateral em casos de lesões traumáticas e tumores para beneficiários com diagnóstico firmado em uma mama, mesmo que a outra ainda esteja saudável.</p>

                      <p>Nesse caso, ela também está intimamente relacionada a uma questão de saúde física e emocional (câncer de mama, depressão, inclusão na sociedade, etc).</p>

                      <h4>Há outros casos em que o plano de saúde pode cobrir cirurgia plástica?</h4>

                      <p>Sim, é possível. A própria blefaroplastia, que é hoje a terceira cirurgia plástica mais procurada no país, pode ter uma indicação clínica, por exemplo.</p>

                      <p> Isso ocorre quando a pálpebra está tão caída que interfere seriamente na visão, causando riscos para o beneficiário. No entanto, é necessária a indicação médica e entrar com um processo na operadora pedindo a realização do procedimento, que deverá ser analisado pelo plano de saúde.</p>

                      <p> Caso a operadora discorde da indicação do médico, um terceiro médico escolhido, de comum acordo por dois outros profissionais, deve ser consultado para a decisão final, ficando a remuneração deste profissional a cargo da operadora, de acordo com o o disposto da RN 319/2013 da ANS.</p>

                      <p> Mas é bom o consumidor ficar preparado para a (forte) possibilidade de ter que entrar na Justiça para pleitear a realização da cirurgia. </p>


                      <figure class="alignleft">
                        <img src="../img/blog/post_img_8.jpg" alt="">
                        <figcaption>O que é obrigatório?</figcaption>
                      </figure>

                      <p> Outro caso em que a cobertura de cirurgia plástica é obrigatória é no caso de órteses, próteses e seus acessórios ligados ao ato cirúrgico, nos planos com cobertura para internação hospitalar. desde que não tenham finalidade estética.</p>

                      <p>Quando a cirurgia plástica tem apenas finalidade estética, ou seja, é eletiva, como uma lipoaspiração, por exemplo, o beneficiário conseguirá apenas cobertura para os exames pré-operatórios e a consulta.</p>

                      <p>O restante, como gastos hospitalares e equipe médica, não são contemplados pelos planos.</p>

                      <p>Por isso, a recomendação é sempre conversar bastante e de forma sincera com o vendedor do plano de saúde e ler atentamente todas as cláusulas do contrato antes de assiná-lo.</p>

                      <p>Consulte as operadoras que atuam na sua região e analise atentamente o que cada plano de saúde tem a oferecer.</p>

                      <p>Faça uma simulação online para saber qual deles melhor se encaixa no seu orçamento e converse com nossos vendedores especializados.</p>

                      <p>Mas não deixe para a hora da necessidade, porque nunca se sabe quando ela surgirá.</p>


                    </div> <!-- end entry article -->
                  </div> <!-- end entry article wrap -->
                </article>
                
                <section class="related-posts">
                  <h5 class="mb-24">Veja Também</h5>
                  <div class="row row-16 card-row">
                    <div class="col-lg-4">
                      <article class="entry card card--small box-shadow hover-up">
                        <div class="entry__img-holder card__img-holder">
                          <a href="plano-de-saude-carencia-zero.php">
                            <img src="../img/blog/post_1.jpg" class="entry__img" alt="">
                          </a>
                          <div class="entry__body card__body">
                            <h4 class="entry__title">
                              <a href="plano-de-saude-carencia-zero.php">Plano de Saúde carência zero existe ou não?</a>
                            </h4>
                          </div>
                        </div>
                      </article>
                    </div>
                    <div class="col-lg-4">
                      <article class="entry card card--small box-shadow hover-up">
                        <div class="entry__img-holder card__img-holder">
                          <a href="como-fazer-um-plano-de-saude-empresarial.php">
                            <img src="../img/blog/post_2.jpg" class="entry__img" alt="">
                          </a>
                          <div class="entry__body card__body">
                            <h4 class="entry__title">
                              <a href="como-fazer-um-plano-de-saude-empresarial.php">Como fazer um Plano de Saúde Empresarial</a>
                            </h4>
                          </div>
                        </div>
                      </article>
                    </div>
                    <div class="col-lg-4">
                      <article class="entry card card--small box-shadow hover-up">
                        <div class="entry__img-holder card__img-holder">
                          <a href="plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">
                            <img src="../img/blog/post_3.jpg" class="entry__img" alt="">
                          </a>
                          <div class="entry__body card__body">
                            <h4 class="entry__title">
                              <a href="plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">Plano de Saúde cobre ou não cirurgia plástica?
                              </a>
                            </h4>
                          </div>
                        </div>
                      </article>
                    </div>
                  </div>
                </section>
                

                <!-- Comments -->



                <!-- Prev / Next Post -->
                <nav class="entry-navigation">
                  <div class="row">
                    <div class="col-lg-6">
                      <a href="como-fazer-um-plano-de-saude-empresarial.php" class="entry-navigation__url entry-navigation--left">
                        <img src="../img/blog/next_post-pme.jpg" alt="" class="entry-navigation__img">
                        <div class="entry-navigation__body">
                          <i class="ui-arrow-left"></i>
                          <span class="entry-navigation__label">Post Anterior</span>
                          <h6 class="entry-navigation__title">Como fazer um Plano de Saúde Empresarial</h6>
                        </div>
                      </a>
                    </div>
                    <div class="col-lg-6">
                      <a href="plano-de-saude-carencia-zero.php" class="entry-navigation__url entry-navigation--right">
                        <div class="entry-navigation__body">
                          <span class="entry-navigation__label">Próximo Post</span>
                          <i class="ui-arrow-right"></i>
                          <h6 class="entry-navigation__title">Plano de Saúde carência zero existe ou não?</h6>
                        </div>
                        <img src="../img/blog/next_post-carencias.jpg" alt="" class="entry-navigation__img">
                      </a>
                    </div>
                  </div>                  
                </nav> 

              </div>
            </div>
          </div>
        </div>
      </section> <!-- end single post -->


      <!-- Newsletter -->
      <section class="section-wrap newsletter bg-color-overlay--2" style="background-image: url(../img/newsletter/newsletter.jpg);">
        <div class="container">
          <div class="title-row title-row--boxed text-center">
            <h3 class="section-title">Cote online</h3>
            <p class="subtitle">Preencha o formulário e cote online</p>
          </div>
          <div class="widget widget_mc4wp_form_widget">
            <div class="newsletter__container">
              <div class="newsletter__form">
                  <?php include("../includes/IncludesInsereBanco.php"); ?>
                  <form class="optin__form" role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #fff">
                      <?php echo $msgClientesSucesso; ?>
                      <?php echo $msgClientesErro; ?>
                      <?php echo $e; ?>
                      <?php include("../includes/includeForm.php"); ?>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </section> <!-- end newsletter -->


      <!-- Footer -->
      <footer class="footer">
        <div class="container">
          <div class="footer__widgets">
            <div class="row">

              <div class="col-lg-3 col-md-6">
                <div class="widget widget-about-us">
                  <!-- Logo -->
                  <a href="index.php" class="logo-container flex-child">
                    <img class="logo" src="../img/logo.png" srcset="../img/logo.png 1x, ../img/logo@2x.png 2x" alt="logo">
                  </a>
                  <p class="mt-24 mb-32">Encontre o plano de saúde que mais se encaixa na sua necessidade.</p>
                  <div class="socials">
                    <a href="#" class="social social-twitter" aria-label="twitter" title="twitter" target="_blank"><i class="ui-twitter"></i></a>
                    <a href="#" class="social social-facebook" aria-label="facebook" title="facebook" target="_blank"><i class="ui-facebook"></i></a>
                    <a href="#" class="social social-google-plus" aria-label="google plus" title="google plus" target="_blank"><i class="ui-google"></i></a>
                  </div>
                </div>
              </div> <!-- end about us -->


              <div class="col-lg-2 offset-lg-5 col-md-6">
                <div class="widget widget_nav_menu">
                  <h5 class="widget-title">Saúde 360</h5>
                  <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Planos</a></li>
                    <li><a href="#">Sobre</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contato</a></li>
                  </ul>
                </div>
              </div>



              <div class="col-lg-2 col-md-6">
                <div class="widget widget-address">
                  <h5 class="widget-title">Operadoras</h5>
                  <ul>
                    <li><a href="#">Amil</a></li>
                    <li><a href="#">Bradesco</a></li>
                    <li><a href="#">Sulamerica</a></li>
                    <li><a href="#">Intermedica</a></li>
                  </ul>
                </div>
              </div>

            </div>
          </div>
        </div> <!-- end container -->

        <div class="footer__bottom top-divider">
          <div class="container text-center">
            <span class="copyright">
              &copy; 2018 Saúde 360, Desenvolvido por <a href="https://agenciatresmeiazero.com.br">Agência TresMeiaZero</a>
            </span>
          </div>
        </div> <!-- end footer bottom -->
      </footer> <!-- end footer -->

      <div id="back-to-top">
        <a href="#top"><i class="ui-arrow-up"></i></a>
      </div>

    </div> <!-- end content wrapper -->
  </main> <!-- end main wrapper -->



  <?php include("../includes/includesFooter.php"); ?>


  
</body>
</html>