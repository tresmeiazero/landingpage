<form role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #000">
  <div class="card-body text-left ">
  <div class="row">
      <div class="col-md-12">
        <?php echo $msgClientesSucesso; ?>
        <?php echo $msgClientesErro; ?>
        <?php echo $e; ?>
          <div class="alert alert-info" role="alert">
              <div class="container">
                  <strong>Dica:</strong> Quem possui CNPJ economiza até <strong>30%</strong> nos Planos de Saúde à partir de 2 vidas!
              </div>
          </div>
      </div>
      <div class="col-md-12">
          <label>Você possui CNPJ?</label>
          <select class="selectpicker" data-style="btn btn-round btn-simple" title="Insira uma opção"  name="possuicnpj" id="possuicnpj" onclick="craateUserJsObject.ShowPrivileges();" required>
              <option selected disabled>Selecione</option>
              <option value="Possuo CNPJ" id="Pme">Sim</option>
              <option value="Nao Possuo CNPJ">Não</option>
          </select>
      </div>

      <div class="resources col-md-12" style=" display: none;">
          <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label>CNPJ ou Razão Social</label>
                      <input type="text" class="form-control" placeholder="Insira seu CNPJ ou Razão Social" name="cnpj" id="cnpj">
                  </div>
              </div>
              <div class="col-md-6" >
                  <label>Quantidade de vidas</label>
                  <select class="selectpicker" data-style="btn btn-round btn-simple" title="Insira uma opção"  name="modalidade" id="modalidade">

                  </select>
              </div>
          </div>
      </div>

      <div class="resources2 col-md-12" style=" display: none;padding-bottom: 25px">
          <label>Selecione o tipo de plano</label>


          <div class="row">
              <div class="col-md-6">
                  <div class="form-check form-check-radio">
                      <label class="form-check-label">

                          <input class="radio-unput" type="radio" name="modalidade" id="tipodeplano1" value="Adesao" onclick="craateUserJsObject.ShowPrivileges();">
                          <span class="form-check-sign"></span>
                          <label for="tipodeplano1">Individual</label>
                      </label>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-check form-check-radio">
                      <label class="form-check-label">

                          <input class="radio-unput" type="radio" name="modalidade" id="tipodeplano2" value="Familiar">
                          <span class="form-check-sign"></span>
                          <label for="tipodeplano2">Familiar</label>

                      </label>
                  </div>
              </div>
          </div>
      </div>


      <div class="resources3 col-md-12" style=" display: none;padding-bottom: 25px">


          <label>Selecione uma das alternativas</label>

          <div class="row">
              <div class="col-md-4">
                  <div class="form-check form-check-radio">
                      <label class="form-check-label">
                          <input class="form-check-input radio-unput" type="radio" name="tipopessoa" id="gestante" value="Gestante">
                          <span class="form-check-sign"></span>
                          <label for="gestante">Gestante</label>

                      </label>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-check form-check-radio">
                      <label class="form-check-label">
                          <input class="form-check-input radio-unput" type="radio" name="tipopessoa" id="idoso" value="Idoso">
                          <span class="form-check-sign"></span>
                          <label for="idoso">Idoso</label>
                      </label>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-check form-check-radio">
                      <label class="form-check-label">
                          <input class="form-check-input radio-unput" type="radio" name="tipopessoa" id="nenhum" value="Uma vida">
                          <span class="form-check-sign"></span>
                          <label for="nenhum">Nenhum dos dois</label>

                      </label>
                  </div>
              </div>
          </div>
      </div>

      <div class="resources4 col-md-12" style=" display: none;">
          <div class="row">
              <div class="col-md-12" >
                  <label>Quantidade de vidas</label>
                  <select  data-style="btn btn-round btn-simple " class="selectpicker"  name="quantidadefamiliar" id="quantidadefamiliar">

                  </select>
              </div>
          </div>
      </div>


      <div class="col-md-6">
          <div class="form-group">
              <label>Nome</label>
              <input type="text" class="form-control" placeholder="Insira seu nome..." aria-label="Insira seu nome..." name="nome" id="nome" required>
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
              <label>E-mail</label>
              <input type="email" class="form-control" placeholder="Insira seu E-mail..." name="email" id="email" required>
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
              <label>Celular</label>
              <input  type="text" class="form-control phone_with_ddd" placeholder="Insira com DDD"  name="telefone" id="telefone" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
              <label>Telefone Alternativo</label>
              <input type="text" class="form-control phone" placeholder="Insira com DDD" name="telefoneAlternativo" id="telefoneAlternativo" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
          </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-6">
          <label for="estado" class="col-form-label">Estado</label>
          <select id="estado" name="estado"  data-style="btn btn-simple btn-round" class="selectpicker"   data-size="7" required >
              <option> Choose</option>
          </select>
      </div>
      <div class="col-md-6">
          <label for="cidade" class="col-form-label">Cidade</label>
          <select id="cidade" name="cidade" data-style="btn btn-simple btn-round" class="selectpicker"   data-size="7" required >
              <option> Choose</option>
          </select>
      </div>
      <div class="col-md-12">
          <div class="form-group">
              <label>Deixe uma mensagem sobre o que precisa (Opcional)</label>
              <textarea name="mensagem" class="form-control" id="message" rows="6" ></textarea>
          </div>
      </div>
      <div class="d-none">
          <div class="input-group">
              <input  type="text" class="form-control" value="Next" name="operadoraNext" id="operadora">
          </div>
      </div>
  </div>
  <div class="row">
            <div class="col-md-12">
                <button type="submit" id="cadastrar" name="cadastrar" class="btn btn-warning btn-lg pull-right">Solicitar Cotação</button>
            </div>
        </div>
</div>

</form>
