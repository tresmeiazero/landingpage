<link href="https://fonts.googleapis.com/css?family=Khand:300,400,500,600,700" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/css/demo.css" rel="stylesheet" />
<link href="assets/css/formValidation.min.css" rel="stylesheet" />
<script src="assets/js/plugins/sweetalert2.all.js"></script>
<!-- Hotjar Tracking Code for planosdesaude360.com.br -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:917565,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

